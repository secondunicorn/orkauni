//
//  FeedViewController.swift
//  Orka
//
//  Created by YR on 12/6/18.
//  Copyright © 2018 Orka. All rights reserved.
//

import UIKit
import AWSMobileClient
import AWSCognitoIdentityProvider

class FeedViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    

    
    
    
    
    
    
    
    
    @IBAction func searchClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Search", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as UIViewController
        
        self.present(controller, animated: true, completion: nil)
    }
    
    
    @IBAction func createChallengeTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Challenge", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChallengeViewController") as UIViewController
        
        self.present(controller, animated: true, completion: nil)
    }
    
    
}
