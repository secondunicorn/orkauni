//
//  PreviewChallengeViewController.swift
//  Orka
//
//  Created by YR on 12/8/18.
//  Copyright © 2018 Orka. All rights reserved.
//

import UIKit

class PreviewChallengeViewController: UIViewController {

    @IBOutlet weak var photo: UIImageView!
    var image:UIImage!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photo.image = self.image

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
