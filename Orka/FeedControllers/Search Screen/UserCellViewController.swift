//
//  UserCellViewController.swift
//  Orka
//
//  Created by YR on 12/6/18.
//  Copyright © 2018 Orka. All rights reserved.
//

import UIKit


protocol UserCellDelegate {
    func didTapAddUser(cellUserID:String)
}

class UserCellViewController: UITableViewCell {
    
    @IBOutlet weak var labelViewCell: UILabel!
    @IBOutlet weak var imageViewCell: UIImageView!

    var delegate: UserCellDelegate?
    var userItem: User?
    
    func setUser(user: User){
        userItem = user
        labelViewCell.text = user.username
    }
    
    @IBAction func addUserTapped(_ sender: Any) {
        print("deep debuggin ",userItem?.sub)
        delegate?.didTapAddUser(cellUserID: (userItem?.sub)!)
    }
}

