//
//  File.swift
//  Orka
//
//  Created by YR on 12/6/18.
//  Copyright © 2018 Orka. All rights reserved.
//

import Foundation
import UIKit

class User {
    
    //var image: UIImage
    let username: String
    let sub : String
    
    init(username: String, sub:String){//},image: UIImage){
        //self.image = image
        self.username = username
        self.sub = sub
    }
}
