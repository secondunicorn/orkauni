//
//  SearchViewController.swift
//  Orka
//
//  Created by YR on 12/6/18.
//  Copyright © 2018 Orka. All rights reserved.
//

import UIKit
import AWSAppSync
import AWSCognitoIdentityProvider

class SearchViewController: UIViewController , UISearchBarDelegate{
   
    //Reference AppSync client
    var appSyncClient: AWSAppSyncClient?
    
    var users: [User] = []
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Reference AppSync client from App Delegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appSyncClient = appDelegate.appSyncClient
        
        //users = createArray()
        
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        //view.endEditing(true)
    }
 
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        let input = TableStringFilterInput(contains: searchBar.text!.lowercased())
        let filter = TableUserFilterInput(preferredusername: input)
        //Run a query
        appSyncClient?.fetch(query: ListUsersQuery(filter: filter), cachePolicy: .fetchIgnoringCacheData) { (result, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }
            print("the searched query result for ",searchBar.text, " is: ", result?.data?.listUsers?.items)
            
            
            
            if let items =  result?.data?.listUsers?.items {
                //print("printing the attributes", attributes)//?.dictionaryValue["username"])
                
                for item in items {
                    
                    //let user = User(username: (item?.preferredusername!)!)
                    //self.users.append(user)
                    self.createArray(username: (item?.preferredusername)!, sub:(item?.id)! )

                }
                self.tableView.reloadData()
            }
        }
        

    }
    
    
    
    
    
    
    
    
    
    

    
    func createArray(username: String!, sub:String) -> () {
        //var tempUser: [User] = []
        
        let user1 = User(username: username, sub: sub)
        //let user2 = User(username: "user2")
        
        users.append(user1)
        //users.append(user2)
        
        //return tempUser
    }
    
    
    
    
    
 

}



extension SearchViewController: UserCellDelegate{
    func didTapAddUser(cellUserID: String) {
        print("the sub of the user is ", cellUserID)
        
        getUserID(cellUserID: cellUserID)
       // registerFriendInviteMutation(sender: myID, target: cellUserID)
        
    }
 
}




extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = users[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell") as! UserCellViewController!
        
        cell?.setUser(user: user)
        cell?.delegate = self
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("clicked on index: ",indexPath.row)
    }
    
    
    
    
    
    func registerFriendInviteMutation(sender: String, target:String){
        
        let mutationInput = CreateFriendInviteInput(senderId: sender, receiverId: target)
        
        appSyncClient?.perform(mutation: CreateFriendInviteMutation(input: mutationInput)) { (result, error) in
            if let error = error as? AWSAppSyncClientError {
                print("Error occurred: \(error.localizedDescription )")
            }
            if let resultError = result?.errors {
                print("Error saving the item on server: \(resultError)")
                return
            }
            print(sender," invited: ", target)
        }
        
    }
    

    //get user cognito user pool attribute
    func getUserID(cellUserID: String){
        
        let pool = AWSCognitoIdentityUserPool.default()
        let user = pool.currentUser()
        
        user?.getDetails().continueOnSuccessWith { (task) -> AnyObject in // handle all auth setup
            DispatchQueue.main.async(execute: {
                let response = task.result // AWSCognitoIdentityUserGetDetailsResponse
                
                if let attributes = task.result?.userAttributes {
                    //print("printing the attributes", attributes)//?.dictionaryValue["username"])
                    
                    for attribute in attributes {
                        
                        print(attribute.name," ",attribute.value)
                        
                        if attribute.name == "sub" {
                            
                            if (attribute.value != cellUserID){ //check if im not sending the invite to myself
                            self.registerFriendInviteMutation(sender: attribute.value!, target: cellUserID)
                            }
                            
                        }
                    }
                }
                
            })
            
            return task
        }
        
        
    }
    
    
    
}

