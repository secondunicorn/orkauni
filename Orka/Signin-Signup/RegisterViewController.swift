//
//  RegisterViewController.swift
//  Orka
//
//  Created by YR on 11/19/18.
//  Copyright © 2018 Orka. All rights reserved.
//

import UIKit
import AWSMobileClient

class RegisterViewController: UIViewController {

    @IBOutlet weak var Name: UITextView!
    @IBOutlet weak var Email: UITextView!
    @IBOutlet weak var Password: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func onRegisterClicked(_ sender: Any) {
        if( Name.hasText && Email.hasText && Password.hasText){
            let name_text:String = Name.text!
            let email_text:String = Email.text!
            let password_text:String = Password.text!
            
            registerNewUser(email: email_text, password: password_text, name:name_text)
            
        }
    }
    
    
                    //Register new user
    
    func registerNewUser(email: String, password: String, name: String){
        print("passed varables : +",email," ",password," ",name)
        AWSMobileClient.sharedInstance().signUp(username: email,
            password: password,
            userAttributes: ["email":email, "preferred_username": name]) { (signUpResult, error) in
                if let signUpResult = signUpResult {
                    switch(signUpResult.signUpConfirmationState) {
                    case .confirmed:
                        DispatchQueue.main.async {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let controller = storyboard.instantiateViewController(withIdentifier: "MainViewController") as UIViewController
                            
                            self.present(controller, animated: true, completion: nil)
                        }
                    
                        break
                    
                    case .unconfirmed:
                        //Unconfirmed email
                        print("User is not confirmed and needs verification via \(signUpResult.codeDeliveryDetails!.deliveryMedium) sent at \(signUpResult.codeDeliveryDetails!.destination!)")
                        
                    case .unknown:
                        print("Unexpected case")
                        break
                    }
                } else if let error = error {
                    if let error = error as? AWSMobileClientError {
                        switch(error) {
                        case .usernameExists(let message):
                            print(message)
                        default:
                            break
                        }
                    }
                    print("\(error.localizedDescription)")
                }
        }
    }
    
    
}

