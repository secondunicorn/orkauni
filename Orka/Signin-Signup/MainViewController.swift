//
//  ViewController.swift
//  Orka
//
//  Created by YR on 11/17/18.
//  Copyright © 2018 Orka. All rights reserved.
//

import UIKit
import AWSMobileClient
import FacebookCore
import FacebookLogin
import FBSDKCoreKit
import AWSAppSync


class MainViewController: UIViewController {

    var appSyncClient: AWSAppSyncClient?
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Reference AppSync client from App Delegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appSyncClient = appDelegate.appSyncClient
        
        //check user state
        initializeState()
        
        
        
    }

    
    //check state, if signed in go to feed directly
    func initializeState() -> Void {
        AWSMobileClient.sharedInstance().initialize { (userState, error) in
            if let userState = userState {
                print("UserState: \(userState.rawValue)")
                
                switch (userState){
                case .signedIn:
                    DispatchQueue.main.async {
                        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as UIViewController
                        
                        self.present(controller, animated: true, completion: nil)
                    }
                    break
                case .signedOut:
                    break
                case .signedOutFederatedTokensInvalid:
                    break
                case .signedOutUserPoolsTokenInvalid:
                    break
                case .guest:
                    break
                case .unknown:
                    break
                }
            } else if let error = error {
                print("error: \(error.localizedDescription)")
            }
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    
   
    @IBAction func onSigninClicked(_ sender: Any) {
       
      signInUser()
            
    }
 

        func signInUser() {
        if( emailField.hasText && passwordField.hasText){
            let email = emailField.text
            let password = passwordField.text
            AWSMobileClient.sharedInstance().signIn(username: email! , password: password!) { (signInResult, error) in
                if let error = error  {
                    print("\(error.localizedDescription)")
                } else if let signInResult = signInResult {
                    
                    //user signed in manually after clicking the button
                    switch (signInResult.signInState) {
                        
                    case .signedIn:
                        
                        print("User is signed in")
                        
                         DispatchQueue.main.async {
                         
                         let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                         let controller = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as UIViewController
                         
                         self.present(controller, animated: true, completion: nil)
                         
                         print("User is signed in.")
                         }
                         
                    case .smsMFA:
                        print("SMS message sent to \(signInResult.codeDetails!.destination!)")
                    default:
                        print("Sign In needs info which is not et supported.")
                    }
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    @IBAction func signinFacebook(_ sender: Any) {
        facebookLoginButtonClicked()
/*
        AWSMobileClient.sharedInstance().federatedSignIn(providerName: IdentityProvider.facebook.rawValue, token: "254738775198721|GUYK3lhXmKZbySvTVSGARpWjjhY") { (userState, error)  in
            
        
            if let userState = userState {
                print("UserState: \(userState.rawValue)")
                
                switch (userState){
                case .signedIn:
                    DispatchQueue.main.async {
                        print("federated signed in from Facebook !")
                        let storyboard = UIStoryboard(name: "Feed", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as UIViewController
                        
                        self.present(controller, animated: true, completion: nil)
                    }
                    break
                case .signedOut:
                    break
                case .signedOutFederatedTokensInvalid:
                    break
                case .signedOutUserPoolsTokenInvalid:
                    break
                case .guest:
                    break
                case .unknown:
                    break
                }
            } else if let error = error {
                print("Federated Sign In failed: \(error.localizedDescription)")
            }
        
            
            
        }
    */
    
    }
        
                                            //fb login clicked, redirect
    func facebookLoginButtonClicked() {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile, .email, .userFriends], viewController: self) {
            (LoginResult) in
            switch LoginResult {
                case .failed(let Error):
                    print(Error)
                    break
                case .cancelled:
                    print("User cancelled login.")
                    break
                case .success(let grantedPermissions, let declinedPermissions, let token):
                    print("Logged in!")
                    
                    if((FBSDKAccessToken.current()) != nil){
                        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler:
                            { (connection, result, error) -> Void in
                            if (error == nil){
                                let field = result! as? [String:Any]
                                
                                print("the identities out of Facebook are: ",result)
                                
                            
                                let email = field!["email"] as! String
                                print("the email out of Facebook is: ", email)
                                
                                let name = field!["name"] as! String
                                print("the name out of Facebook is: ", name)
                                //print("the first_name out of Facebook is: ",field!["first_name"])
                                //print("the last_name out of Facebook is: ",field!["last_name"])
                                let id = field!["id"] as! String
                                self.registerNewUser(email: email, password: email+name, name: name)
                                
                            }
                            
                        })
                    }
                    break
            }
        }
        
    }
    
    
    
    //Register Facebook User
    
    func registerNewUser(email: String, password: String, name: String){
        print("passed varables : +",email," ",password," ",name)
        AWSMobileClient.sharedInstance().signUp(username: email, password: password, userAttributes: ["email":email, "preferred_username": name]) { (signUpResult, error) in
            if let signUpResult = signUpResult {
                switch(signUpResult.signUpConfirmationState) {
                case .confirmed:
                    DispatchQueue.main.async {
                        print("User added to database from facebook")
                        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as UIViewController
                        
                        self.present(controller, animated: true, completion: nil)
                    }
                    
                    break
                    
                case .unconfirmed:
                    //Unconfirmed email
                    print("User is not confirmed and needs verification via \(signUpResult.codeDeliveryDetails!.deliveryMedium) sent at \(signUpResult.codeDeliveryDetails!.destination!)")
                    
                case .unknown:
                    print("Unexpected case")
                    break
                }
            } else if let error = error {
                if let error = error as? AWSMobileClientError {
                    switch(error) {
                    case .usernameExists(let message):
                        print(message)
                        DispatchQueue.main.async {
                            print("User exists in the database from facebook")
                            let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                            let controller = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as UIViewController
                            
                            self.present(controller, animated: true, completion: nil)
                        }
                    default:
                        break
                    }
                }
                print("\(error.localizedDescription)")
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

                                            //Register view controller
    @IBAction func onRegisterClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Register", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as UIViewController
        
        self.present(controller, animated: true, completion: nil)
    }
    
    
    
    
    
    
   
        
}




