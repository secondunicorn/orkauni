//
//  ProfileViewController.swift
//  Orka
//
//  Created by YR on 11/19/18.
//  Copyright © 2018 Orka. All rights reserved.
//

import UIKit
import AWSMobileClient
import AWSAppSync
import AWSCognitoIdentityProvider

class ProfileViewController: UIViewController{
    
    
    @IBOutlet weak var UserNameField: UITextView!
    @IBOutlet weak var PointsField: UILabel!
    @IBOutlet weak var UpvotesField: UILabel!
    @IBOutlet weak var StarsField: UILabel!
    
    

    
    @IBOutlet weak var ConnectionsField: UILabel!
    @IBOutlet weak var FansField: UILabel!
    
    
    var response: AWSCognitoIdentityUserGetDetailsResponse?
    var user: AWSCognitoIdentityUser?
    var pool: AWSCognitoIdentityUserPool?
    
    //Reference AppSync client
    var appSyncClient: AWSAppSyncClient?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        //Reference AppSync client from App Delegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appSyncClient = appDelegate.appSyncClient
        
        
        print("started activity")
   
        getUserID()
        
        //UserInfoQuery(sub:"d5732c10-065e-461f-8319-cb2a242fdbd0")
        
        //UserInfoQuery()
   
       

    }
 
    
    

    
    @IBAction func feedClicked(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Feed", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "FeedViewController") as UIViewController
        
        self.present(controller, animated: true, completion: nil)
        
    }
    
    
    
 
    @IBAction func signoutClicked(_ sender: Any) {
        AWSMobileClient.sharedInstance().signOut()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MainViewController") as UIViewController
        
        self.present(controller, animated: true, completion: nil)
    }
    
   
    
    
    
    
    //get user cognito user pool attribute
    func getUserID(){
        
        pool = AWSCognitoIdentityUserPool.default()
        user = pool?.currentUser()
        
        self.user?.getDetails().continueOnSuccessWith { (task) -> AnyObject in // handle all auth setup
        DispatchQueue.main.async(execute: {
        self.response = task.result // AWSCognitoIdentityUserGetDetailsResponse
        
            if let attributes = task.result?.userAttributes {
               //print("printing the attributes", attributes)//?.dictionaryValue["username"])

                for attribute in attributes {

                print(attribute.name," ",attribute.value)

                    if attribute.name == "sub" {

                        self.UserInfoQuery(sub: attribute.value!)
                    }
                }
            }
        
        })
        
        return task
        }

        
    }
    
    
    func UserInfoQuery(sub:String){

        print("the sub past is: ",sub)
        //Run a query
        appSyncClient?.fetch(query: GetUserQuery(id: sub), cachePolicy: .fetchIgnoringCacheData)  { (result, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }
            //result?.data?.listTodos?.items!.forEach { print(($0?.name)! + " " + ($0?.description)!) }
            print("thisis the query result: ", result?.data)
            
            
        
            var pts = String((result?.data?.getUser?.points!)!)
            self.PointsField.text = pts
            
            pts = String((result?.data?.getUser?.upvotes!)!)
            self.UpvotesField.text = pts
            
            pts = String((result?.data?.getUser?.stars!)!)
            self.StarsField.text = pts
            
            pts = String((result?.data?.getUser?.connections!)!)
            self.ConnectionsField.text = pts
            
            pts = String((result?.data?.getUser?.fans!)!)
            self.FansField.text = pts
            
            pts = String((result?.data?.getUser?.preferredusername!)!)
            self.UserNameField.text = pts
            
            
            
        }
        


    }
    
    
}


