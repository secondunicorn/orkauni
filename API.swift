//  This file was automatically generated and should not be edited.

import AWSAppSync

public struct CreateUserInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil, upvotes: Int? = nil, stars: Int? = nil, connections: Int? = nil, fans: Int? = nil) {
    graphQLMap = ["email": email, "preferredusername": preferredusername, "date": date, "points": points, "upvotes": upvotes, "stars": stars, "connections": connections, "fans": fans]
  }

  public var email: String? {
    get {
      return graphQLMap["email"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var preferredusername: String? {
    get {
      return graphQLMap["preferredusername"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "preferredusername")
    }
  }

  public var date: String? {
    get {
      return graphQLMap["date"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "date")
    }
  }

  public var points: Int? {
    get {
      return graphQLMap["points"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "points")
    }
  }

  public var upvotes: Int? {
    get {
      return graphQLMap["upvotes"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "upvotes")
    }
  }

  public var stars: Int? {
    get {
      return graphQLMap["stars"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "stars")
    }
  }

  public var connections: Int? {
    get {
      return graphQLMap["connections"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "connections")
    }
  }

  public var fans: Int? {
    get {
      return graphQLMap["fans"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "fans")
    }
  }
}

public struct UpdateUserInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: String, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil, upvotes: Int? = nil, stars: Int? = nil, connections: Int? = nil, fans: Int? = nil) {
    graphQLMap = ["id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points, "upvotes": upvotes, "stars": stars, "connections": connections, "fans": fans]
  }

  public var id: String {
    get {
      return graphQLMap["id"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var email: String? {
    get {
      return graphQLMap["email"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var preferredusername: String? {
    get {
      return graphQLMap["preferredusername"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "preferredusername")
    }
  }

  public var date: String? {
    get {
      return graphQLMap["date"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "date")
    }
  }

  public var points: Int? {
    get {
      return graphQLMap["points"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "points")
    }
  }

  public var upvotes: Int? {
    get {
      return graphQLMap["upvotes"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "upvotes")
    }
  }

  public var stars: Int? {
    get {
      return graphQLMap["stars"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "stars")
    }
  }

  public var connections: Int? {
    get {
      return graphQLMap["connections"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "connections")
    }
  }

  public var fans: Int? {
    get {
      return graphQLMap["fans"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "fans")
    }
  }
}

public struct DeleteUserInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: String) {
    graphQLMap = ["id": id]
  }

  public var id: String {
    get {
      return graphQLMap["id"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct CreateFriendInviteInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(senderId: GraphQLID, receiverId: GraphQLID) {
    graphQLMap = ["senderID": senderId, "receiverID": receiverId]
  }

  public var senderId: GraphQLID {
    get {
      return graphQLMap["senderId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "senderId")
    }
  }

  public var receiverId: GraphQLID {
    get {
      return graphQLMap["receiverId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "receiverId")
    }
  }
}

public struct UpdateFriendInviteInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(senderId: GraphQLID, receiverId: GraphQLID) {
    graphQLMap = ["senderID": senderId, "receiverID": receiverId]
  }

  public var senderId: GraphQLID {
    get {
      return graphQLMap["senderId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "senderId")
    }
  }

  public var receiverId: GraphQLID {
    get {
      return graphQLMap["receiverId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "receiverId")
    }
  }
}

public struct DeleteFriendInviteInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(senderId: GraphQLID, receiverId: GraphQLID) {
    graphQLMap = ["senderID": senderId, "receiverID": receiverId]
  }

  public var senderId: GraphQLID {
    get {
      return graphQLMap["senderId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "senderId")
    }
  }

  public var receiverId: GraphQLID {
    get {
      return graphQLMap["receiverId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "receiverId")
    }
  }
}

public struct TableUserFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: TableStringFilterInput? = nil, email: TableStringFilterInput? = nil, preferredusername: TableStringFilterInput? = nil, date: TableStringFilterInput? = nil, points: TableIntFilterInput? = nil, upvotes: TableIntFilterInput? = nil, stars: TableIntFilterInput? = nil, connections: TableIntFilterInput? = nil, fans: TableIntFilterInput? = nil) {
    graphQLMap = ["id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points, "upvotes": upvotes, "stars": stars, "connections": connections, "fans": fans]
  }

  public var id: TableStringFilterInput? {
    get {
      return graphQLMap["id"] as! TableStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var email: TableStringFilterInput? {
    get {
      return graphQLMap["email"] as! TableStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var preferredusername: TableStringFilterInput? {
    get {
      return graphQLMap["preferredusername"] as! TableStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "preferredusername")
    }
  }

  public var date: TableStringFilterInput? {
    get {
      return graphQLMap["date"] as! TableStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "date")
    }
  }

  public var points: TableIntFilterInput? {
    get {
      return graphQLMap["points"] as! TableIntFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "points")
    }
  }

  public var upvotes: TableIntFilterInput? {
    get {
      return graphQLMap["upvotes"] as! TableIntFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "upvotes")
    }
  }

  public var stars: TableIntFilterInput? {
    get {
      return graphQLMap["stars"] as! TableIntFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "stars")
    }
  }

  public var connections: TableIntFilterInput? {
    get {
      return graphQLMap["connections"] as! TableIntFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "connections")
    }
  }

  public var fans: TableIntFilterInput? {
    get {
      return graphQLMap["fans"] as! TableIntFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "fans")
    }
  }
}

public struct TableStringFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: String? = nil, eq: String? = nil, le: String? = nil, lt: String? = nil, ge: String? = nil, gt: String? = nil, contains: String? = nil, notContains: String? = nil, between: [String?]? = nil, beginsWith: String? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: String? {
    get {
      return graphQLMap["ne"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: String? {
    get {
      return graphQLMap["eq"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: String? {
    get {
      return graphQLMap["le"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: String? {
    get {
      return graphQLMap["lt"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: String? {
    get {
      return graphQLMap["ge"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: String? {
    get {
      return graphQLMap["gt"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: String? {
    get {
      return graphQLMap["contains"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: String? {
    get {
      return graphQLMap["notContains"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [String?]? {
    get {
      return graphQLMap["between"] as! [String?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: String? {
    get {
      return graphQLMap["beginsWith"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public struct TableIntFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: Int? = nil, eq: Int? = nil, le: Int? = nil, lt: Int? = nil, ge: Int? = nil, gt: Int? = nil, contains: Int? = nil, notContains: Int? = nil, between: [Int?]? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between]
  }

  public var ne: Int? {
    get {
      return graphQLMap["ne"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: Int? {
    get {
      return graphQLMap["eq"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: Int? {
    get {
      return graphQLMap["le"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: Int? {
    get {
      return graphQLMap["lt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: Int? {
    get {
      return graphQLMap["ge"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: Int? {
    get {
      return graphQLMap["gt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: Int? {
    get {
      return graphQLMap["contains"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: Int? {
    get {
      return graphQLMap["notContains"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [Int?]? {
    get {
      return graphQLMap["between"] as! [Int?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }
}

public struct TableFriendInviteFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(senderId: TableIDFilterInput? = nil, receiverId: TableIDFilterInput? = nil) {
    graphQLMap = ["senderID": senderId, "receiverID": receiverId]
  }

  public var senderId: TableIDFilterInput? {
    get {
      return graphQLMap["senderId"] as! TableIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "senderId")
    }
  }

  public var receiverId: TableIDFilterInput? {
    get {
      return graphQLMap["receiverId"] as! TableIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "receiverId")
    }
  }
}

public struct TableIDFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: GraphQLID? = nil, eq: GraphQLID? = nil, le: GraphQLID? = nil, lt: GraphQLID? = nil, ge: GraphQLID? = nil, gt: GraphQLID? = nil, contains: GraphQLID? = nil, notContains: GraphQLID? = nil, between: [GraphQLID?]? = nil, beginsWith: GraphQLID? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: GraphQLID? {
    get {
      return graphQLMap["ne"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: GraphQLID? {
    get {
      return graphQLMap["eq"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: GraphQLID? {
    get {
      return graphQLMap["le"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: GraphQLID? {
    get {
      return graphQLMap["lt"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: GraphQLID? {
    get {
      return graphQLMap["ge"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: GraphQLID? {
    get {
      return graphQLMap["gt"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: GraphQLID? {
    get {
      return graphQLMap["contains"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: GraphQLID? {
    get {
      return graphQLMap["notContains"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [GraphQLID?]? {
    get {
      return graphQLMap["between"] as! [GraphQLID?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: GraphQLID? {
    get {
      return graphQLMap["beginsWith"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public final class CreateSssssUserMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateSssssUser($input: String!) {\n  createSSSSSUser(input: $input)\n}"

  public var input: String

  public init(input: String) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createSSSSSUser", arguments: ["input": GraphQLVariable("input")], type: .scalar(String.self)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createSssssUser: String? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createSSSSSUser": createSssssUser])
    }

    public var createSssssUser: String? {
      get {
        return snapshot["createSSSSSUser"] as? String
      }
      set {
        snapshot.updateValue(newValue, forKey: "createSSSSSUser")
      }
    }
  }
}

public final class CreateUserMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateUser($input: CreateUserInput!) {\n  createUser(input: $input) {\n    __typename\n    id\n    email\n    preferredusername\n    date\n    points\n    upvotes\n    stars\n    connections\n    fans\n  }\n}"

  public var input: CreateUserInput

  public init(input: CreateUserInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createUser", arguments: ["input": GraphQLVariable("input")], type: .object(CreateUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createUser: CreateUser? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createUser": createUser.flatMap { $0.snapshot }])
    }

    public var createUser: CreateUser? {
      get {
        return (snapshot["createUser"] as? Snapshot).flatMap { CreateUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createUser")
      }
    }

    public struct CreateUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("email", type: .scalar(String.self)),
        GraphQLField("preferredusername", type: .scalar(String.self)),
        GraphQLField("date", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("upvotes", type: .scalar(Int.self)),
        GraphQLField("stars", type: .scalar(Int.self)),
        GraphQLField("connections", type: .scalar(Int.self)),
        GraphQLField("fans", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil, upvotes: Int? = nil, stars: Int? = nil, connections: Int? = nil, fans: Int? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points, "upvotes": upvotes, "stars": stars, "connections": connections, "fans": fans])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String? {
        get {
          return snapshot["email"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var preferredusername: String? {
        get {
          return snapshot["preferredusername"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredusername")
        }
      }

      public var date: String? {
        get {
          return snapshot["date"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "date")
        }
      }

      public var points: Int? {
        get {
          return snapshot["points"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "points")
        }
      }

      public var upvotes: Int? {
        get {
          return snapshot["upvotes"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "upvotes")
        }
      }

      public var stars: Int? {
        get {
          return snapshot["stars"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "stars")
        }
      }

      public var connections: Int? {
        get {
          return snapshot["connections"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "connections")
        }
      }

      public var fans: Int? {
        get {
          return snapshot["fans"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "fans")
        }
      }
    }
  }
}

public final class UpdateUserMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateUser($input: UpdateUserInput!) {\n  updateUser(input: $input) {\n    __typename\n    id\n    email\n    preferredusername\n    date\n    points\n    upvotes\n    stars\n    connections\n    fans\n  }\n}"

  public var input: UpdateUserInput

  public init(input: UpdateUserInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateUser", arguments: ["input": GraphQLVariable("input")], type: .object(UpdateUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateUser: UpdateUser? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateUser": updateUser.flatMap { $0.snapshot }])
    }

    public var updateUser: UpdateUser? {
      get {
        return (snapshot["updateUser"] as? Snapshot).flatMap { UpdateUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateUser")
      }
    }

    public struct UpdateUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("email", type: .scalar(String.self)),
        GraphQLField("preferredusername", type: .scalar(String.self)),
        GraphQLField("date", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("upvotes", type: .scalar(Int.self)),
        GraphQLField("stars", type: .scalar(Int.self)),
        GraphQLField("connections", type: .scalar(Int.self)),
        GraphQLField("fans", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil, upvotes: Int? = nil, stars: Int? = nil, connections: Int? = nil, fans: Int? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points, "upvotes": upvotes, "stars": stars, "connections": connections, "fans": fans])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String? {
        get {
          return snapshot["email"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var preferredusername: String? {
        get {
          return snapshot["preferredusername"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredusername")
        }
      }

      public var date: String? {
        get {
          return snapshot["date"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "date")
        }
      }

      public var points: Int? {
        get {
          return snapshot["points"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "points")
        }
      }

      public var upvotes: Int? {
        get {
          return snapshot["upvotes"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "upvotes")
        }
      }

      public var stars: Int? {
        get {
          return snapshot["stars"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "stars")
        }
      }

      public var connections: Int? {
        get {
          return snapshot["connections"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "connections")
        }
      }

      public var fans: Int? {
        get {
          return snapshot["fans"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "fans")
        }
      }
    }
  }
}

public final class DeleteUserMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteUser($input: DeleteUserInput!) {\n  deleteUser(input: $input) {\n    __typename\n    id\n    email\n    preferredusername\n    date\n    points\n    upvotes\n    stars\n    connections\n    fans\n  }\n}"

  public var input: DeleteUserInput

  public init(input: DeleteUserInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteUser", arguments: ["input": GraphQLVariable("input")], type: .object(DeleteUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteUser: DeleteUser? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteUser": deleteUser.flatMap { $0.snapshot }])
    }

    public var deleteUser: DeleteUser? {
      get {
        return (snapshot["deleteUser"] as? Snapshot).flatMap { DeleteUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteUser")
      }
    }

    public struct DeleteUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("email", type: .scalar(String.self)),
        GraphQLField("preferredusername", type: .scalar(String.self)),
        GraphQLField("date", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("upvotes", type: .scalar(Int.self)),
        GraphQLField("stars", type: .scalar(Int.self)),
        GraphQLField("connections", type: .scalar(Int.self)),
        GraphQLField("fans", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil, upvotes: Int? = nil, stars: Int? = nil, connections: Int? = nil, fans: Int? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points, "upvotes": upvotes, "stars": stars, "connections": connections, "fans": fans])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String? {
        get {
          return snapshot["email"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var preferredusername: String? {
        get {
          return snapshot["preferredusername"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredusername")
        }
      }

      public var date: String? {
        get {
          return snapshot["date"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "date")
        }
      }

      public var points: Int? {
        get {
          return snapshot["points"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "points")
        }
      }

      public var upvotes: Int? {
        get {
          return snapshot["upvotes"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "upvotes")
        }
      }

      public var stars: Int? {
        get {
          return snapshot["stars"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "stars")
        }
      }

      public var connections: Int? {
        get {
          return snapshot["connections"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "connections")
        }
      }

      public var fans: Int? {
        get {
          return snapshot["fans"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "fans")
        }
      }
    }
  }
}

public final class CreateFriendInviteMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateFriendInvite($input: CreateFriendInviteInput!) {\n  createFriendInvite(input: $input) {\n    __typename\n    senderID\n    receiverID\n  }\n}"

  public var input: CreateFriendInviteInput

  public init(input: CreateFriendInviteInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createFriendInvite", arguments: ["input": GraphQLVariable("input")], type: .object(CreateFriendInvite.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createFriendInvite: CreateFriendInvite? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createFriendInvite": createFriendInvite.flatMap { $0.snapshot }])
    }

    public var createFriendInvite: CreateFriendInvite? {
      get {
        return (snapshot["createFriendInvite"] as? Snapshot).flatMap { CreateFriendInvite(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createFriendInvite")
      }
    }

    public struct CreateFriendInvite: GraphQLSelectionSet {
      public static let possibleTypes = ["FriendInvite"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("senderID", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("receiverID", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(senderId: GraphQLID, receiverId: GraphQLID) {
        self.init(snapshot: ["__typename": "FriendInvite", "senderID": senderId, "receiverID": receiverId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var senderId: GraphQLID {
        get {
          return snapshot["senderID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "senderID")
        }
      }

      public var receiverId: GraphQLID {
        get {
          return snapshot["receiverID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "receiverID")
        }
      }
    }
  }
}

public final class UpdateFriendInviteMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateFriendInvite($input: UpdateFriendInviteInput!) {\n  updateFriendInvite(input: $input) {\n    __typename\n    senderID\n    receiverID\n  }\n}"

  public var input: UpdateFriendInviteInput

  public init(input: UpdateFriendInviteInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateFriendInvite", arguments: ["input": GraphQLVariable("input")], type: .object(UpdateFriendInvite.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateFriendInvite: UpdateFriendInvite? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateFriendInvite": updateFriendInvite.flatMap { $0.snapshot }])
    }

    public var updateFriendInvite: UpdateFriendInvite? {
      get {
        return (snapshot["updateFriendInvite"] as? Snapshot).flatMap { UpdateFriendInvite(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateFriendInvite")
      }
    }

    public struct UpdateFriendInvite: GraphQLSelectionSet {
      public static let possibleTypes = ["FriendInvite"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("senderID", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("receiverID", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(senderId: GraphQLID, receiverId: GraphQLID) {
        self.init(snapshot: ["__typename": "FriendInvite", "senderID": senderId, "receiverID": receiverId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var senderId: GraphQLID {
        get {
          return snapshot["senderID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "senderID")
        }
      }

      public var receiverId: GraphQLID {
        get {
          return snapshot["receiverID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "receiverID")
        }
      }
    }
  }
}

public final class DeleteFriendInviteMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteFriendInvite($input: DeleteFriendInviteInput!) {\n  deleteFriendInvite(input: $input) {\n    __typename\n    senderID\n    receiverID\n  }\n}"

  public var input: DeleteFriendInviteInput

  public init(input: DeleteFriendInviteInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteFriendInvite", arguments: ["input": GraphQLVariable("input")], type: .object(DeleteFriendInvite.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteFriendInvite: DeleteFriendInvite? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteFriendInvite": deleteFriendInvite.flatMap { $0.snapshot }])
    }

    public var deleteFriendInvite: DeleteFriendInvite? {
      get {
        return (snapshot["deleteFriendInvite"] as? Snapshot).flatMap { DeleteFriendInvite(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteFriendInvite")
      }
    }

    public struct DeleteFriendInvite: GraphQLSelectionSet {
      public static let possibleTypes = ["FriendInvite"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("senderID", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("receiverID", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(senderId: GraphQLID, receiverId: GraphQLID) {
        self.init(snapshot: ["__typename": "FriendInvite", "senderID": senderId, "receiverID": receiverId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var senderId: GraphQLID {
        get {
          return snapshot["senderID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "senderID")
        }
      }

      public var receiverId: GraphQLID {
        get {
          return snapshot["receiverID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "receiverID")
        }
      }
    }
  }
}

public final class GetSssssUserQuery: GraphQLQuery {
  public static let operationString =
    "query GetSssssUser($id: String!) {\n  getSSSSSUser(id: $id)\n}"

  public var id: String

  public init(id: String) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getSSSSSUser", arguments: ["id": GraphQLVariable("id")], type: .scalar(String.self)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getSssssUser: String? = nil) {
      self.init(snapshot: ["__typename": "Query", "getSSSSSUser": getSssssUser])
    }

    public var getSssssUser: String? {
      get {
        return snapshot["getSSSSSUser"] as? String
      }
      set {
        snapshot.updateValue(newValue, forKey: "getSSSSSUser")
      }
    }
  }
}

public final class GetUserQuery: GraphQLQuery {
  public static let operationString =
    "query GetUser($id: String!) {\n  getUser(id: $id) {\n    __typename\n    id\n    email\n    preferredusername\n    date\n    points\n    upvotes\n    stars\n    connections\n    fans\n  }\n}"

  public var id: String

  public init(id: String) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getUser", arguments: ["id": GraphQLVariable("id")], type: .object(GetUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getUser: GetUser? = nil) {
      self.init(snapshot: ["__typename": "Query", "getUser": getUser.flatMap { $0.snapshot }])
    }

    public var getUser: GetUser? {
      get {
        return (snapshot["getUser"] as? Snapshot).flatMap { GetUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getUser")
      }
    }

    public struct GetUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("email", type: .scalar(String.self)),
        GraphQLField("preferredusername", type: .scalar(String.self)),
        GraphQLField("date", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("upvotes", type: .scalar(Int.self)),
        GraphQLField("stars", type: .scalar(Int.self)),
        GraphQLField("connections", type: .scalar(Int.self)),
        GraphQLField("fans", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil, upvotes: Int? = nil, stars: Int? = nil, connections: Int? = nil, fans: Int? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points, "upvotes": upvotes, "stars": stars, "connections": connections, "fans": fans])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String? {
        get {
          return snapshot["email"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var preferredusername: String? {
        get {
          return snapshot["preferredusername"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredusername")
        }
      }

      public var date: String? {
        get {
          return snapshot["date"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "date")
        }
      }

      public var points: Int? {
        get {
          return snapshot["points"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "points")
        }
      }

      public var upvotes: Int? {
        get {
          return snapshot["upvotes"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "upvotes")
        }
      }

      public var stars: Int? {
        get {
          return snapshot["stars"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "stars")
        }
      }

      public var connections: Int? {
        get {
          return snapshot["connections"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "connections")
        }
      }

      public var fans: Int? {
        get {
          return snapshot["fans"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "fans")
        }
      }
    }
  }
}

public final class ListUsersQuery: GraphQLQuery {
  public static let operationString =
    "query ListUsers($filter: TableUserFilterInput, $limit: Int, $nextToken: String) {\n  listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      id\n      email\n      preferredusername\n      date\n      points\n      upvotes\n      stars\n      connections\n      fans\n    }\n    nextToken\n  }\n}"

  public var filter: TableUserFilterInput?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: TableUserFilterInput? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listUsers", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listUsers: ListUser? = nil) {
      self.init(snapshot: ["__typename": "Query", "listUsers": listUsers.flatMap { $0.snapshot }])
    }

    public var listUsers: ListUser? {
      get {
        return (snapshot["listUsers"] as? Snapshot).flatMap { ListUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listUsers")
      }
    }

    public struct ListUser: GraphQLSelectionSet {
      public static let possibleTypes = ["UserConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "UserConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(String.self))),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("preferredusername", type: .scalar(String.self)),
          GraphQLField("date", type: .scalar(String.self)),
          GraphQLField("points", type: .scalar(Int.self)),
          GraphQLField("upvotes", type: .scalar(Int.self)),
          GraphQLField("stars", type: .scalar(Int.self)),
          GraphQLField("connections", type: .scalar(Int.self)),
          GraphQLField("fans", type: .scalar(Int.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: String, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil, upvotes: Int? = nil, stars: Int? = nil, connections: Int? = nil, fans: Int? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points, "upvotes": upvotes, "stars": stars, "connections": connections, "fans": fans])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String {
          get {
            return snapshot["id"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String? {
          get {
            return snapshot["email"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var preferredusername: String? {
          get {
            return snapshot["preferredusername"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredusername")
          }
        }

        public var date: String? {
          get {
            return snapshot["date"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "date")
          }
        }

        public var points: Int? {
          get {
            return snapshot["points"] as? Int
          }
          set {
            snapshot.updateValue(newValue, forKey: "points")
          }
        }

        public var upvotes: Int? {
          get {
            return snapshot["upvotes"] as? Int
          }
          set {
            snapshot.updateValue(newValue, forKey: "upvotes")
          }
        }

        public var stars: Int? {
          get {
            return snapshot["stars"] as? Int
          }
          set {
            snapshot.updateValue(newValue, forKey: "stars")
          }
        }

        public var connections: Int? {
          get {
            return snapshot["connections"] as? Int
          }
          set {
            snapshot.updateValue(newValue, forKey: "connections")
          }
        }

        public var fans: Int? {
          get {
            return snapshot["fans"] as? Int
          }
          set {
            snapshot.updateValue(newValue, forKey: "fans")
          }
        }
      }
    }
  }
}

public final class GetFriendInviteQuery: GraphQLQuery {
  public static let operationString =
    "query GetFriendInvite($receiverID: ID!, $senderID: ID!) {\n  getFriendInvite(receiverID: $receiverID, senderID: $senderID) {\n    __typename\n    senderID\n    receiverID\n  }\n}"

  public var receiverID: GraphQLID
  public var senderID: GraphQLID

  public init(receiverID: GraphQLID, senderID: GraphQLID) {
    self.receiverID = receiverID
    self.senderID = senderID
  }

  public var variables: GraphQLMap? {
    return ["receiverID": receiverID, "senderID": senderID]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getFriendInvite", arguments: ["receiverID": GraphQLVariable("receiverID"), "senderID": GraphQLVariable("senderID")], type: .object(GetFriendInvite.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getFriendInvite: GetFriendInvite? = nil) {
      self.init(snapshot: ["__typename": "Query", "getFriendInvite": getFriendInvite.flatMap { $0.snapshot }])
    }

    public var getFriendInvite: GetFriendInvite? {
      get {
        return (snapshot["getFriendInvite"] as? Snapshot).flatMap { GetFriendInvite(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getFriendInvite")
      }
    }

    public struct GetFriendInvite: GraphQLSelectionSet {
      public static let possibleTypes = ["FriendInvite"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("senderID", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("receiverID", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(senderId: GraphQLID, receiverId: GraphQLID) {
        self.init(snapshot: ["__typename": "FriendInvite", "senderID": senderId, "receiverID": receiverId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var senderId: GraphQLID {
        get {
          return snapshot["senderID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "senderID")
        }
      }

      public var receiverId: GraphQLID {
        get {
          return snapshot["receiverID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "receiverID")
        }
      }
    }
  }
}

public final class ListFriendInvitesQuery: GraphQLQuery {
  public static let operationString =
    "query ListFriendInvites($filter: TableFriendInviteFilterInput, $limit: Int, $nextToken: String) {\n  listFriendInvites(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      senderID\n      receiverID\n    }\n    nextToken\n  }\n}"

  public var filter: TableFriendInviteFilterInput?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: TableFriendInviteFilterInput? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listFriendInvites", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListFriendInvite.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listFriendInvites: ListFriendInvite? = nil) {
      self.init(snapshot: ["__typename": "Query", "listFriendInvites": listFriendInvites.flatMap { $0.snapshot }])
    }

    public var listFriendInvites: ListFriendInvite? {
      get {
        return (snapshot["listFriendInvites"] as? Snapshot).flatMap { ListFriendInvite(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listFriendInvites")
      }
    }

    public struct ListFriendInvite: GraphQLSelectionSet {
      public static let possibleTypes = ["FriendInviteConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "FriendInviteConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["FriendInvite"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("senderID", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("receiverID", type: .nonNull(.scalar(GraphQLID.self))),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(senderId: GraphQLID, receiverId: GraphQLID) {
          self.init(snapshot: ["__typename": "FriendInvite", "senderID": senderId, "receiverID": receiverId])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var senderId: GraphQLID {
          get {
            return snapshot["senderID"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "senderID")
          }
        }

        public var receiverId: GraphQLID {
          get {
            return snapshot["receiverID"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "receiverID")
          }
        }
      }
    }
  }
}

public final class OnCreateUserSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateUser($id: String, $email: String, $preferredusername: String, $date: String, $points: Int) {\n  onCreateUser(id: $id, email: $email, preferredusername: $preferredusername, date: $date, points: $points) {\n    __typename\n    id\n    email\n    preferredusername\n    date\n    points\n    upvotes\n    stars\n    connections\n    fans\n  }\n}"

  public var id: String?
  public var email: String?
  public var preferredusername: String?
  public var date: String?
  public var points: Int?

  public init(id: String? = nil, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil) {
    self.id = id
    self.email = email
    self.preferredusername = preferredusername
    self.date = date
    self.points = points
  }

  public var variables: GraphQLMap? {
    return ["id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateUser", arguments: ["id": GraphQLVariable("id"), "email": GraphQLVariable("email"), "preferredusername": GraphQLVariable("preferredusername"), "date": GraphQLVariable("date"), "points": GraphQLVariable("points")], type: .object(OnCreateUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateUser: OnCreateUser? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateUser": onCreateUser.flatMap { $0.snapshot }])
    }

    public var onCreateUser: OnCreateUser? {
      get {
        return (snapshot["onCreateUser"] as? Snapshot).flatMap { OnCreateUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateUser")
      }
    }

    public struct OnCreateUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("email", type: .scalar(String.self)),
        GraphQLField("preferredusername", type: .scalar(String.self)),
        GraphQLField("date", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("upvotes", type: .scalar(Int.self)),
        GraphQLField("stars", type: .scalar(Int.self)),
        GraphQLField("connections", type: .scalar(Int.self)),
        GraphQLField("fans", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil, upvotes: Int? = nil, stars: Int? = nil, connections: Int? = nil, fans: Int? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points, "upvotes": upvotes, "stars": stars, "connections": connections, "fans": fans])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String? {
        get {
          return snapshot["email"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var preferredusername: String? {
        get {
          return snapshot["preferredusername"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredusername")
        }
      }

      public var date: String? {
        get {
          return snapshot["date"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "date")
        }
      }

      public var points: Int? {
        get {
          return snapshot["points"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "points")
        }
      }

      public var upvotes: Int? {
        get {
          return snapshot["upvotes"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "upvotes")
        }
      }

      public var stars: Int? {
        get {
          return snapshot["stars"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "stars")
        }
      }

      public var connections: Int? {
        get {
          return snapshot["connections"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "connections")
        }
      }

      public var fans: Int? {
        get {
          return snapshot["fans"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "fans")
        }
      }
    }
  }
}

public final class OnUpdateUserSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdateUser($id: String, $email: String, $preferredusername: String, $date: String, $points: Int) {\n  onUpdateUser(id: $id, email: $email, preferredusername: $preferredusername, date: $date, points: $points) {\n    __typename\n    id\n    email\n    preferredusername\n    date\n    points\n    upvotes\n    stars\n    connections\n    fans\n  }\n}"

  public var id: String?
  public var email: String?
  public var preferredusername: String?
  public var date: String?
  public var points: Int?

  public init(id: String? = nil, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil) {
    self.id = id
    self.email = email
    self.preferredusername = preferredusername
    self.date = date
    self.points = points
  }

  public var variables: GraphQLMap? {
    return ["id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdateUser", arguments: ["id": GraphQLVariable("id"), "email": GraphQLVariable("email"), "preferredusername": GraphQLVariable("preferredusername"), "date": GraphQLVariable("date"), "points": GraphQLVariable("points")], type: .object(OnUpdateUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdateUser: OnUpdateUser? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdateUser": onUpdateUser.flatMap { $0.snapshot }])
    }

    public var onUpdateUser: OnUpdateUser? {
      get {
        return (snapshot["onUpdateUser"] as? Snapshot).flatMap { OnUpdateUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdateUser")
      }
    }

    public struct OnUpdateUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("email", type: .scalar(String.self)),
        GraphQLField("preferredusername", type: .scalar(String.self)),
        GraphQLField("date", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("upvotes", type: .scalar(Int.self)),
        GraphQLField("stars", type: .scalar(Int.self)),
        GraphQLField("connections", type: .scalar(Int.self)),
        GraphQLField("fans", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil, upvotes: Int? = nil, stars: Int? = nil, connections: Int? = nil, fans: Int? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points, "upvotes": upvotes, "stars": stars, "connections": connections, "fans": fans])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String? {
        get {
          return snapshot["email"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var preferredusername: String? {
        get {
          return snapshot["preferredusername"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredusername")
        }
      }

      public var date: String? {
        get {
          return snapshot["date"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "date")
        }
      }

      public var points: Int? {
        get {
          return snapshot["points"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "points")
        }
      }

      public var upvotes: Int? {
        get {
          return snapshot["upvotes"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "upvotes")
        }
      }

      public var stars: Int? {
        get {
          return snapshot["stars"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "stars")
        }
      }

      public var connections: Int? {
        get {
          return snapshot["connections"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "connections")
        }
      }

      public var fans: Int? {
        get {
          return snapshot["fans"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "fans")
        }
      }
    }
  }
}

public final class OnDeleteUserSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeleteUser($id: String, $email: String, $preferredusername: String, $date: String, $points: Int) {\n  onDeleteUser(id: $id, email: $email, preferredusername: $preferredusername, date: $date, points: $points) {\n    __typename\n    id\n    email\n    preferredusername\n    date\n    points\n    upvotes\n    stars\n    connections\n    fans\n  }\n}"

  public var id: String?
  public var email: String?
  public var preferredusername: String?
  public var date: String?
  public var points: Int?

  public init(id: String? = nil, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil) {
    self.id = id
    self.email = email
    self.preferredusername = preferredusername
    self.date = date
    self.points = points
  }

  public var variables: GraphQLMap? {
    return ["id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeleteUser", arguments: ["id": GraphQLVariable("id"), "email": GraphQLVariable("email"), "preferredusername": GraphQLVariable("preferredusername"), "date": GraphQLVariable("date"), "points": GraphQLVariable("points")], type: .object(OnDeleteUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeleteUser: OnDeleteUser? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeleteUser": onDeleteUser.flatMap { $0.snapshot }])
    }

    public var onDeleteUser: OnDeleteUser? {
      get {
        return (snapshot["onDeleteUser"] as? Snapshot).flatMap { OnDeleteUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeleteUser")
      }
    }

    public struct OnDeleteUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("email", type: .scalar(String.self)),
        GraphQLField("preferredusername", type: .scalar(String.self)),
        GraphQLField("date", type: .scalar(String.self)),
        GraphQLField("points", type: .scalar(Int.self)),
        GraphQLField("upvotes", type: .scalar(Int.self)),
        GraphQLField("stars", type: .scalar(Int.self)),
        GraphQLField("connections", type: .scalar(Int.self)),
        GraphQLField("fans", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, email: String? = nil, preferredusername: String? = nil, date: String? = nil, points: Int? = nil, upvotes: Int? = nil, stars: Int? = nil, connections: Int? = nil, fans: Int? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "preferredusername": preferredusername, "date": date, "points": points, "upvotes": upvotes, "stars": stars, "connections": connections, "fans": fans])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String? {
        get {
          return snapshot["email"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var preferredusername: String? {
        get {
          return snapshot["preferredusername"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredusername")
        }
      }

      public var date: String? {
        get {
          return snapshot["date"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "date")
        }
      }

      public var points: Int? {
        get {
          return snapshot["points"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "points")
        }
      }

      public var upvotes: Int? {
        get {
          return snapshot["upvotes"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "upvotes")
        }
      }

      public var stars: Int? {
        get {
          return snapshot["stars"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "stars")
        }
      }

      public var connections: Int? {
        get {
          return snapshot["connections"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "connections")
        }
      }

      public var fans: Int? {
        get {
          return snapshot["fans"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "fans")
        }
      }
    }
  }
}

public final class OnCreateFriendInviteSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateFriendInvite($senderID: ID, $receiverID: ID) {\n  onCreateFriendInvite(senderID: $senderID, receiverID: $receiverID) {\n    __typename\n    senderID\n    receiverID\n  }\n}"

  public var senderID: GraphQLID?
  public var receiverID: GraphQLID?

  public init(senderID: GraphQLID? = nil, receiverID: GraphQLID? = nil) {
    self.senderID = senderID
    self.receiverID = receiverID
  }

  public var variables: GraphQLMap? {
    return ["senderID": senderID, "receiverID": receiverID]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateFriendInvite", arguments: ["senderID": GraphQLVariable("senderID"), "receiverID": GraphQLVariable("receiverID")], type: .object(OnCreateFriendInvite.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateFriendInvite: OnCreateFriendInvite? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateFriendInvite": onCreateFriendInvite.flatMap { $0.snapshot }])
    }

    public var onCreateFriendInvite: OnCreateFriendInvite? {
      get {
        return (snapshot["onCreateFriendInvite"] as? Snapshot).flatMap { OnCreateFriendInvite(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateFriendInvite")
      }
    }

    public struct OnCreateFriendInvite: GraphQLSelectionSet {
      public static let possibleTypes = ["FriendInvite"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("senderID", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("receiverID", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(senderId: GraphQLID, receiverId: GraphQLID) {
        self.init(snapshot: ["__typename": "FriendInvite", "senderID": senderId, "receiverID": receiverId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var senderId: GraphQLID {
        get {
          return snapshot["senderID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "senderID")
        }
      }

      public var receiverId: GraphQLID {
        get {
          return snapshot["receiverID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "receiverID")
        }
      }
    }
  }
}

public final class OnUpdateFriendInviteSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdateFriendInvite($senderID: ID, $receiverID: ID) {\n  onUpdateFriendInvite(senderID: $senderID, receiverID: $receiverID) {\n    __typename\n    senderID\n    receiverID\n  }\n}"

  public var senderID: GraphQLID?
  public var receiverID: GraphQLID?

  public init(senderID: GraphQLID? = nil, receiverID: GraphQLID? = nil) {
    self.senderID = senderID
    self.receiverID = receiverID
  }

  public var variables: GraphQLMap? {
    return ["senderID": senderID, "receiverID": receiverID]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdateFriendInvite", arguments: ["senderID": GraphQLVariable("senderID"), "receiverID": GraphQLVariable("receiverID")], type: .object(OnUpdateFriendInvite.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdateFriendInvite: OnUpdateFriendInvite? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdateFriendInvite": onUpdateFriendInvite.flatMap { $0.snapshot }])
    }

    public var onUpdateFriendInvite: OnUpdateFriendInvite? {
      get {
        return (snapshot["onUpdateFriendInvite"] as? Snapshot).flatMap { OnUpdateFriendInvite(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdateFriendInvite")
      }
    }

    public struct OnUpdateFriendInvite: GraphQLSelectionSet {
      public static let possibleTypes = ["FriendInvite"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("senderID", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("receiverID", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(senderId: GraphQLID, receiverId: GraphQLID) {
        self.init(snapshot: ["__typename": "FriendInvite", "senderID": senderId, "receiverID": receiverId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var senderId: GraphQLID {
        get {
          return snapshot["senderID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "senderID")
        }
      }

      public var receiverId: GraphQLID {
        get {
          return snapshot["receiverID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "receiverID")
        }
      }
    }
  }
}

public final class OnDeleteFriendInviteSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeleteFriendInvite($senderID: ID, $receiverID: ID) {\n  onDeleteFriendInvite(senderID: $senderID, receiverID: $receiverID) {\n    __typename\n    senderID\n    receiverID\n  }\n}"

  public var senderID: GraphQLID?
  public var receiverID: GraphQLID?

  public init(senderID: GraphQLID? = nil, receiverID: GraphQLID? = nil) {
    self.senderID = senderID
    self.receiverID = receiverID
  }

  public var variables: GraphQLMap? {
    return ["senderID": senderID, "receiverID": receiverID]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeleteFriendInvite", arguments: ["senderID": GraphQLVariable("senderID"), "receiverID": GraphQLVariable("receiverID")], type: .object(OnDeleteFriendInvite.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeleteFriendInvite: OnDeleteFriendInvite? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeleteFriendInvite": onDeleteFriendInvite.flatMap { $0.snapshot }])
    }

    public var onDeleteFriendInvite: OnDeleteFriendInvite? {
      get {
        return (snapshot["onDeleteFriendInvite"] as? Snapshot).flatMap { OnDeleteFriendInvite(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeleteFriendInvite")
      }
    }

    public struct OnDeleteFriendInvite: GraphQLSelectionSet {
      public static let possibleTypes = ["FriendInvite"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("senderID", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("receiverID", type: .nonNull(.scalar(GraphQLID.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(senderId: GraphQLID, receiverId: GraphQLID) {
        self.init(snapshot: ["__typename": "FriendInvite", "senderID": senderId, "receiverID": receiverId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var senderId: GraphQLID {
        get {
          return snapshot["senderID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "senderID")
        }
      }

      public var receiverId: GraphQLID {
        get {
          return snapshot["receiverID"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "receiverID")
        }
      }
    }
  }
}
